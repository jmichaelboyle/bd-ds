###
# Data Visualization (advanced): part 2
# References:
# 1. Saccilotto, R., "Tutorial ggplot2", http://www.ceb-institute.org/bbs/wp-content/uploads/2011/09/handout_ggplot2.pdf, accessed 2014/09/08
###

# About ggplot2 ####
# "ggplot2 is an R package for producing statistical, or data, graphics, but it is unlike most other graphics packages because it
# has a deep underlying grammar. This grammar, based on the Grammar of Graphics (Wilkinson, 2005), is composed of a set
# of independent components that can be composed in many different ways. [..] Plots can be built up iteratively and edited
# later. A carefuly chosen set of defaults means that most of the time you can produce a publication-quality graphic in
# seconds, but if you do have speical formatting requirements, a comprehensive theming system makes it easy to do what
# you want. [..]
# ggplot2 is designed to work in a layered fashion, starting with a layer showing the raw data then adding layers of annotation
# and statistical summaries. [..]"
# H.Wickham, ggplot2, Use R, DOI 10.1007/978-0-387-98141_1, © Springer Science+Business Media, LLC 2009

# introduction to ggplot2 ####
#install.packages("ggplot2")
library("ggplot2")

# We'll work with two datasets:
# 1. diamonds: has data about diamond quality
# 2. mtcars: has data about cars and fuel efficiency

# examine the diamonds dataset
head(diamonds) # examine the first few lines of the dataset; note this is similar to the linux command "head"
?diamonds # examine the help file for the diamonds dataset
summary(diamonds) # examine the summary statistics for the diamonds dataset

# examine the mtcars dataset
head(mtcars)
?mtcars
summary(mtcars)

### comparison qplot vs ggplot
?qplot # examine the help file for qplot

# qplot histogram
qplot(clarity, data=diamonds, fill=cut, geom="bar")
names(diamonds) # note that clarity is an attribute in the diamonds dataset

# ggplot histogram -> same output
ggplot(diamonds, aes(clarity, fill=cut)) + geom_bar()

### how to use qplot
# scatterplot
qplot(wt, mpg, data=mtcars)

# transform input data with functions
qplot(log(wt), mpg - 10, data=mtcars)

# add aesthetic mapping (i.e. color)
qplot(wt, mpg, data=mtcars, color=qsec)
names(mtcars)
?mtcars # recall that qsec is an attribute in the mtcars dataset indicating the car's quarter-mile time

# change size of points
qplot(wt, mpg, data=mtcars, color=qsec, size=cyl) # size based on number of cylinders
qplot(wt, mpg, data=mtcars, color=qsec, size=I(5)) # static size of 5

# use alpha blending aesthetic; values between 0 (transparent) and 1 (opaque); can be very effective in certain cases
qplot(wt, mpg, data=mtcars, alpha=qsec)

# continuous scale vs. discrete scale
head(mtcars)
qplot(wt, mpg, data=mtcars, colour=cyl)
unique(mtcars$cyl) # what are the unique values for cyl?
qplot(wt, mpg, data=mtcars, colour=factor(cyl)) # make cyl a factor and the legend/color is more effective

# use different aesthetic mappings
qplot(wt, mpg, data=mtcars, shape=factor(cyl))
qplot(wt, mpg, data=mtcars, size=qsec)

# combine mappings 
qplot(wt, mpg, data=mtcars, size=qsec, color=factor(carb))
qplot(wt, mpg, data=mtcars, size=qsec, shape=factor(cyl))
qplot(wt, mpg, data=mtcars, size=qsec, shape=factor(cyl), geom="point") # what does geom do?

# bar-plot
qplot(factor(cyl), data=mtcars, geom="bar") # that's what geom does!

# flip plot by 90°
qplot(factor(cyl), data=mtcars, geom="bar") + coord_flip()

# difference between fill/color bars
qplot(factor(cyl), data=mtcars, geom="bar", fill=factor(cyl))
qplot(factor(cyl), data=mtcars, geom="bar", colour=factor(cyl))

# fill by variable
qplot(factor(cyl), data=mtcars, geom="bar", fill=factor(gear))

# use different display of bars (stacked, dodged, identity)
head(diamonds)
qplot(clarity, data=diamonds, geom="bar", fill=cut, position="stack")
qplot(clarity, data=diamonds, geom="bar", fill=cut, position="dodge")
qplot(clarity, data=diamonds, geom="bar", fill=cut, position="fill")
qplot(clarity, data=diamonds, geom="bar", fill=cut, position="identity") # everything collided and are overlapping!
qplot(clarity, data=diamonds, geom="freqpoly", group=cut, colour=cut, position="identity") # collisions with this geom are okay
qplot(clarity, data=diamonds, geom="freqpoly", group=cut, colour=cut, position="stack") # stacking in this case is likely not desired; be careful

# histogram
qplot(carat, data=diamonds, geom="histogram")

# change binwidth
qplot(carat, data=diamonds, geom="histogram", binwidth=0.1)
qplot(carat, data=diamonds, geom="histogram", binwidth=0.01)

# use geom to combine plots
qplot(wt, mpg, data=mtcars, geom="point")
qplot(wt, mpg, data=mtcars, geom="smooth")
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"))
qplot(wt, mpg, data=mtcars, geom=c("smooth", "point")) # order doesn't matter in this case

# tweaking the smooth plot ("loess"-method: polynomial surface using local fitting)
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"))
# removing standard error
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"), se=FALSE)
# making line more or less wiggly (span: 0-1)
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"), span=0.6)
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"), span=1)
# using linear modelling
qplot(wt, mpg, data=mtcars, geom=c("point", "smooth"), method="lm")

# sometimes qplot doesn't give us what we're looking for
qplot(wt, mpg, data=mtcars, color=factor(cyl), geom=c("smooth", "point")) # not what we wanted

# in this case swith to the more advanced ggplot
ggplot() +
  geom_point(data=mtcars, aes(x=wt, y=mpg, colour=factor(cyl))) +
  geom_smooth(data=mtcars, aes(x=wt, y=mpg))

# ggplot2 official documentation ####
# visit: http://docs.ggplot2.org/current/