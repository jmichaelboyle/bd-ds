########################
# Introduction to Linux
# references:
# 1. http://linuxcommand.org/index.php, accessed 2014/09/01
# 2. http://www.linux-tutorial.info/modules.php?name=MContent&pageid=224, accessed, 2014/09/01
########################

##
# Set up
##
1. Create a Word document called IS-6481_<your-name>_Lab-1.docx.  This is the document that will be used to paste screenshots during the lab.  It will be the file that is submitted to Canvas after the lab has been completed.

##
# Introduction to the shell
##
#Simply put, the shell is a program that takes commands from the keyboard #and gives them to the operating system to perform.[1]
#On most Linux systems a program called bash (which stands for Bourne Again SHell, an enhanced version of the original Unix shell program, sh, written by Steve Bourne) acts as the shell program. [1]

From an open the terminal:
1. Enter a non-existent command. <prompt>$ lkjlkjlkj

2. Get the last (non-existent) command from "command history" and execute it. <prompt>$ <up-arrow-key>

3. Enter a different non-existing command. <prompt>$ asdasdasd

4. Get the second-to-last (non-existent) command from "command history" and execute it. <prompt>$ <up-arrow-key, twice>

5. Explore the command history with by pressing the up and down arrow keys.

6. Enter your name as a command. <prompt>$ <your name>

Submission: Take a screenshot of your terminal screen and paste it to the Word document.

##
# Navigating and exploring file system 
##
1. Clear the terminal window. <prompt>$ clear

2. Print the working directory. <prompt>$ pwd

3. List the files in the working directory. <prompt>$ ls

4. Change to the 'dev' subdirectory. <prompt>$ cd dev

5. List the files in the working directory. <prompt>$ ls

6. Change to the /usr/bin directory using the absolute path. <prompt>$ cd /usr/bin

7. Print the working directory. <prompt>$ pwd

8. Change to the parent directory using the absolute path. <prompt>$ cd /usr

9. Print the working directory. <prompt>$ pwd

10. Change back to the 'bin' subdirectory. <prompt>$ cd bin

11. Print the working directory. <prompt>$ pwd

12. Now change to the parent directory using the relative path. <prompt>$ cd ..

13. Print the working directory. <prompt>$ pwd

14. Change back to the 'bin' subdirectory using the '.' (i.e. current directory) character. <prompt>$ cd ./bin

15. Print the working directory. <prompt>$ pwd

Submission: Take a screenshot of your terminal screen and paste it to the Word document.

##
# Standard directories and files on Linux
##
# see: http://linuxcommand.org/lc3_lts0040.php
# for more detail see: http://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard

Submission: Answer the following in the Word document.
1. In which directory are the scripts to start up services at boot time?
2. In which file will one find the essential information about users of the system?
3. In which directory will one find configuration files for the system?
4. What is the difference between /bin and /usr/bin?
5. In which directory are interesting programs typically installed by individual users?
6. In which directory should log files be stored?
7. What would be the home directory of the user mjones?
8. Provide an example of something in the '/dev' directory.

##
# Manipulating files part 1
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Create a sample file to work with. <prompt>$ echo $JAVA_HOME > java_home.txt

4. List the files in the working directory. <prompt>$ ls

5. List only .txt files in the working directory using wildcard matching. <prompt>$ ls *.txt

6. List only files that start with j or r in the working directory using wildcard matching. <prompt>$ ls [jr]*

7. List only files that start with 'java_home' followed by exactly four characters in the working directory using wildcard matching. <prompt>$ ls java_home????

8. List only files that start with 'java_home' followed by exactly two characters in the working directory using wildcard matching. <prompt>$ ls java_home??

Submission: Take a screenshot of your terminal screen and paste it to the Word document.

##
# Manipulating files part 2
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~
 
3. Copy the java_home.txt file to java_home_2.txt. <prompt>$ cp java_home.txt java_home_2.txt

4. List the files in the working directory. <prompt>$ ls

5. Make a sample directory. <prompt>$ mkdir sample-dir

6. Copy the java_home.txt file to the sample-dir directory. <prompt>$ cp java_home.txt ./sample-dir

7. List the files in the sample-dir directory from the working directory. <prompt>$ ls ./sample-dir

8. List the files in the working directory. <prompt>$ ls 

9. Copy the entire sample-dir to sample-dir_2. <prompt>$ cp -R ./sample-dir ./sample-dir_2

10. List the files in the working directory. <prompt>$ ls

11. List the files in the sample-dir_2 directory from the working directory. <prompt>$ ls ./sample-dir_2

12. List the files in the sample-dir directory from the working directory. <prompt>$ ls ./sample-dir

13. Remove the file java_home_2.txt. <prompt>$ rm -f java_home_2.txt

14. Remove the directory sample-dir and all of its contents. <prompt>$ rm -rf sample-dir

15. List the files in the working directory. <prompt>$ ls

16. Remove the directory sample-dir_2 and all of its contents. <prompt>$ rm -rf sample-dir_2

17. List the files in the working directory. <prompt>$ ls

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off. 

##
# Manipulating files part 3
##
1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~ 

3. List the .txt files in the working directory. <prompt>$ ls *.txt

4. List the contents of the java_home.txt file. <prompt>$ cat java_home.txt

5. Move (rename, in this case) the java_home.txt file to the filename java_home_renamed.txt. <prompt>$ mv java_home.txt java_home_renamed.txt

6. List the files in the working directory. <prompt>$ ls

7. List the contents of the java_home.txt file. <prompt>$ cat java_home_renamed.txt

8. Move (rename, in this case) the java_home_renamed.txt file back to the filename java_home.txt. <prompt>$ mv java_home_renamed.txt java_home.txt

9. List the .txt files in the working directory. <prompt>$ ls *.txt

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Note: the mv command is very similar in structure to the cp command.
 
##
# Common commands
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Determine the type of command 'cd'. <prompt)$ type cd

4. Determine the type of command 'ls'. <prompt)$ type ls

5. Determine the type of command 'cp'. <prompt)$ type cp

6. Determine the type of command 'R'. <prompt)$ type R

7. Determine the type of command 'cat'. <prompt)$ type cat

8. Determine the location of command 'cd'. <prompt)$ which cd

9. Determine the location of command 'ls'. <prompt)$ which ls

10. Determine the location of command 'cp'. <prompt)$ which cp

11. Determine the location of command 'R'. <prompt)$ which R

12. Determine the location of command 'cat'. <prompt)$ which cat

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Optional activities:
* View the manual for the command cp. <prompt>$ man cp 

##
# I/O redirection
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Direct the output from a command to a new file. <prompt>$ ls -1 > directory-contents.txt

4. List the contents of the new file. <prompt>$ cat directory-contents.txt

5. Direct the output from a command to an existing file. <prompt>$ ls -1 >> directory-contents.txt

6. List the contents of the updated file. <prompt>$ cat directory-contents.txt

7. Sort the contents of the directory-contents.txt file. <prompt>$ sort < directory-contents.txt

8. Sort the contents of the directory-contents.txt file and write out to a new file. <prompt>$ sort < directory-contents.txt > directory-contents_sorted.txt

9. List the contents of the new file. <prompt>$ cat directory-contents_sorted.txt

10. Pipe the the output of the ls -1 command to the sort command. <prompt>$ ls -1 | sort

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

##
# Other common commands
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Find unique lines in the sorted file. <prompt>$ uniq < directory-contents_sorted.txt

4. List the first few lines of the new file. <prompt>$ head directory-contents.txt

5. List the last few lines of the new file. <prompt>$ tail directory-contents.txt

6. Search for lines containing 'l'. <prompt>$ grep l directory-contents.txt

7. Search for lines containing 'li'. <prompt>$ grep li directory-contents.txt

8. Substitute all instances of 'li' for 'AAA' and write to a new file. <prompt>$ sed s/li/AAA/ < directory-contents.txt > directory-contents_modified.txt

9. List the first few lines of the new file. <prompt>$ head directory-contents_modified.txt 

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Optional activities:
* Learn more about sed: http://www.grymoire.com/Unix/Sed.html#uh-0
* Learn more about grep: http://www.grymoire.com/unix/Grep.html
* Learn about find command:
http://content.hccfl.edu/pollock/unix/findcmd.htm
http://www.tecmint.com/35-practical-examples-of-linux-find-command/
* Learn about cut command:
http://www.thegeekstuff.com/2013/06/cut-command-examples/
* Learn about awk command:
http://www.grymoire.com/Unix/Awk.html#uh-0

##
# Permissions
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Using the editor, nano, create a new script called hello-world.sh. <prompt>$ nano hello-world.sh
a. Add the following text:

#!/bin/bash
# My first script
echo "Hello World!"

b. Exit and save the file

4. List the new script including the file permissions. <prompt>$ ls -l hello-world.sh

5. Try to run the new script. <prompt>$ ./hello-world.sh

6. Change the permissions on the script to make it read-write-executable for the owner and read-executable for other users. <prompt>$ chmod 755 hello-world.sh

7. Try to run the script again. <prompt>$ ./hello-world.sh

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Optional activities:
* Learn more about chmod
* Learn about sudo
* Learn about su
* Learn about chown and chgrp

##
# Job control
##

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. Look at running processes. <prompt>$ ps

4. Create a process and run it in the background. <prompt>$ top &

5. Look at running processes. <prompt>$ ps
a. Note the process id for the 'top' process

6. Kill the 'top' process. <prompt>$ kill -9 <process id>

7. Look at running processes. <prompt>$ ps

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Optional activities:
* Learn more about ps
* Learn more about kill
* Learn about pgrep
* Learn about pkill
* Learn about top

##
# Managing services
# reference: http://www.cyberciti.biz/faq/check-running-services-in-rhel-redhat-fedora-centoslinux/
## 

1. Clear the terminal window. <prompt>$ clear

2. Change to the user's home directory. <prompt>$ cd ~

3. List all running services. <prompt>$ service --status-all

4. List all known services. <prompt>$ chkconfig --list

5. List services and their open ports. <prompt>$ sudo netstat -tulpn

Submission: Take a screenshot of your terminal screen and paste it to the Word document.  Don't worry if the top of the session is cut off.

Optional activities:
* Examine simple interface to configure services. <prompt>$ sudo ntsysv

##
# Regular expressions
##

Optional activities:
* Learn about and experiment with regular expressions: http://www.regexr.com/

##
# Putting it all together (create and run a shell script)
##

Optional activities:
* Create a script similar to hello-world.sh that automates some task using a sequence of commands
